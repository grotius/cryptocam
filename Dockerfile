FROM python:3.11-slim

MAINTAINER Ruben Grotius <grotius@hacktor.com>

RUN apt update && apt upgrade -y
RUN apt install -y exiftool

RUN useradd -m -d /opt/app appuser
USER appuser
COPY requirements.txt .
RUN python3 -m venv /opt/app/venv
RUN . /opt/app/venv/bin/activate; pip install -r requirements.txt

# Make sure we use the virtualenv:
ENV PATH="/opt/venv/bin:$PATH"
CMD ['/bin/bash']

# TODO lean image
# FROM python:3.11-slim AS build-image
# RUN useradd -m -d /opt/app appuser
# USER appuser
# COPY --from=compile-image --chown=appuser /opt/app/venv /opt/app/venv
