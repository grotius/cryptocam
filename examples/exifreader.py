import argparse

from exif import Image
from pprint import pprint

parser = argparse.ArgumentParser()
parser.add_argument("image", help="Image to process",
                    nargs='?', default="cryptosigner.jpeg")
args = parser.parse_args()

with open(args.image, 'rb') as img_file:
    img = Image(img_file)

if img.has_exif:
    pprint(img.get_all())
else:
    print("No exif data")
