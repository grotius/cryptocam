import argparse
import hashlib

from PIL import Image
from pprint import pprint

parser = argparse.ArgumentParser()
parser.add_argument("image", help="Image to process",
                    nargs='?', default="cryptosigner.jpeg")
args = parser.parse_args()

try:
    img = Image.open(args.image)
except FileNotFoundError as e:
    print(e)
    parser.print_help()
    exit(1)

print(f"img.format: {img.format}; img.format_description: {img.format_description}; img.mode: {img.mode}")
data = img.getdata()
#print(list(data))
print(type(data))
print(len(data))
print(len(img.tobytes('raw')))
print(hashlib.sha256(img.tobytes('raw')).hexdigest())
