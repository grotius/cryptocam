import argparse

from PIL import Image
import piexif
import piexif.helper
from pprint import pprint

parser = argparse.ArgumentParser()
parser.add_argument("image", help="Image to process",
                    nargs='?', default="cryptosigner.jpeg")
args = parser.parse_args()

ipfs_url = 'https://ipfs.io/ipfs/QmetiX4RLt7vukf1tMpcpKVeTme6Un1imrE2fBCi1c79cz'
user_comment = piexif.helper.UserComment.dump(ipfs_url)

try:
    img = Image.open(args.image)
except FileNotFoundError as e:
    print(e)
    parser.print_help()
    exit(1)

exif_dict = piexif.load(img.info["exif"])

#pprint(exif_dict)
if 'Exif' in exif_dict:
    if piexif.ExifIFD.UserComment in exif_dict["Exif"]:
        print(f"User Comment: {str(exif_dict['Exif'][piexif.ExifIFD.UserComment])}")
    else:
        print(f"User Comment not in Exif")
    exif_dict['Exif'][piexif.ExifIFD.UserComment] = user_comment
else:
    print("Exif not in dict")
    exif_dict['Exif'] = {piexif.ExifIFD.UserComment: user_comment}

exif_bytes = piexif.dump(exif_dict)
img.save(args.image, "jpeg", exif=exif_bytes)
