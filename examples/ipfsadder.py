import requests
import json
from argparse import ArgumentParser
from exif import Image
from pprint import pprint

parser = ArgumentParser()
parser.add_argument("image", help="Image to process",
                    nargs='?', default="cryptosigner.jpeg")
args = parser.parse_args()

class Client:
    def __init__(self, host='127.0.0.1', port=5001, base='api/v0', ssl=False):
        self.url = 'https' if ssl else 'http'
        self.url += f"://{host}:{port}/{base}"
    def add(self, file):
        url = f"{self.url}/add"
        response = requests.post(url, files={'file': open(file, 'rb')})
        return json.loads(response.text)
    def cp(self, src, dst):
        url = f"{self.url}/files/cp"
        src = f"/ipfs/{src}"
        dst = f"/{dst}"
        params = {'arg': [src, dst]}
        return requests.post(url, params=params)


try:
    img_file = open(args.image, 'rb')
    img = Image(img_file)
except FileNotFoundError as e:
    print(e)
    parser.print_help()
    exit(1)

if img.has_exif:
    pprint(img.get_all())
else:
    print("No exif data")

# Connect to the IPFS daemon running on the "frost" server
api = Client(host='frost.dmz', port=5001)

# Add the file to IPFS and get the hash of the uploaded file
result = api.add(args.image)
print(result)

# The result will be a dictionary containing the hash of the uploaded file
# You can use the hash to create the URL
h = result['Hash']
url = f"https://ipfs.io/ipfs/{h}"

print(f"url: {url}")

#result = api.cp(h, args.image)
result = api.cp(h, '/flag2.jpg')
print(vars(result))

